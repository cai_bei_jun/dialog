import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/mytext'
import mytext from "../components/mytext";

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'mytext',
      component: mytext
    }
  ]
})
